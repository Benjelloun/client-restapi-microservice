package com.home.clientrestperson.client;

import com.home.clientrestperson.entities.Personne;
import org.junit.Before;
import org.junit.Test;


import java.util.List;

import static org.junit.Assert.*;

public class PersonRestClientImplTest {

    private IPersonRestClient iPersonRestClient;

    @Before
    public void setUp(){
        iPersonRestClient = new PersonRestClientImpl();
    }

    @Test
    public void TestallPersonne(){
        List<Personne> personnes = iPersonRestClient.AllPersonne();
        assertEquals(5, personnes.size());
    }
@Test
    public void createPerson() {
        Personne personne = new Personne();
        personne.setName("TOTO");
        personne.setFamilyName("PAPA");
        personne.setAdress("ADRESS");
        personne.setMail("TOTO.PAPA@gmail.com");
        Personne createdPerson = iPersonRestClient.createPersonne(personne);
        assertTrue(createdPerson.getIdPerson() != null && "TOTO".equals(createdPerson.getName()));
}

}
