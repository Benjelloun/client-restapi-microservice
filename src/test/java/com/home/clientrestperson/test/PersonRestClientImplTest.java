package com.home.clientrestperson.test;

import com.home.clientrestperson.client.IPersonRestClient;
import com.home.clientrestperson.client.PersonRestClientImpl;
import com.home.clientrestperson.entities.Personne;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class PersonRestClientImplTest {
    private IPersonRestClient iPersonRestClient;

    @Before
    public void setUp(){
        iPersonRestClient = new PersonRestClientImpl();
    }

    @Test
    public void testGetAllPersons(){
        List<Personne> personnes = iPersonRestClient.AllPersonne();
       // System.out.println((personnes.size()));
        assertTrue(personnes.size() ==5);

    }
}
