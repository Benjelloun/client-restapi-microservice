package com.home.clientrestperson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientRestPersonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientRestPersonApplication.class, args);
    }

}
