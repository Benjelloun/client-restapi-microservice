package com.home.clientrestperson.client;

import com.home.clientrestperson.entities.Personne;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

import java.util.List;

public class PersonRestClientImpl implements IPersonRestClient {
    private IPersonRestClient personRestClient;
    private static final String BASE_URL = "http://localhost:8090/v1/personne";

    public PersonRestClientImpl() {
        personRestClient = Feign.builder()
                           .encoder(new JacksonEncoder()) //transformer les entities en format JSON
                           .decoder(new JacksonDecoder())
                           .target(IPersonRestClient.class, BASE_URL);
    }

    public Personne createPersonne(Personne personne) {
        return personRestClient.createPersonne(personne);
    }

    public Personne updatePersonne(Personne personne) {
        return personRestClient.updatePersonne(personne);
    }

    public void deletePersonne(Long id) {
        personRestClient.deletePersonne(id);
    }

    public List<Personne> AllPersonne() {
        return personRestClient.AllPersonne();
    }

    public Personne getPersonne(Long id) {
        return personRestClient.getPersonne(id);
    }

    public List<Personne> getByName(String name) {
        return personRestClient.getByName(name);
    }
}
