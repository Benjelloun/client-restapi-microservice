package com.home.clientrestperson.client;


import com.home.clientrestperson.entities.Personne;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Headers("Accept: Application/json")

public interface IPersonRestClient {
    static final String POST = "POST";
    static final String GET = "GET";
    static final String PUT = "PUT";
    static final String DELETE = "DELETE";

    static final String CREATE_PERSON = POST + "/create";
    static final String UPDATE_PERSON = PUT + "/update/{id}";
    static final String DELETE_PERSON = DELETE + "/delete/{id}";
    static final String GET_ALL_PERSONS = GET + "/getAll";
    static final String GET_PERSON_BY_ID = GET+ "/getOnePersonne/{id}";
    static final String GET_ALL_PERSONS_BY_NAME = GET + "/all/byName/{name}";

    @Headers("Content-Type: application/json")
    @RequestLine(CREATE_PERSON)
    Personne createPersonne( Personne personne);

    @Headers("Content-Type: application/json")
    @RequestLine(UPDATE_PERSON)
    Personne updatePersonne(Personne personne);

    @RequestLine(DELETE_PERSON)
    void deletePersonne(@Param(value = "id") Long id);

    @RequestLine(GET_ALL_PERSONS)
    List<Personne> AllPersonne();

    @RequestLine(GET_PERSON_BY_ID)
    Personne getPersonne(Long id);

    @RequestLine(GET_ALL_PERSONS_BY_NAME)
    List<Personne> getByName(String name);
}
