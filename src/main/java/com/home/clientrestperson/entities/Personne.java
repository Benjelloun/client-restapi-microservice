package com.home.clientrestperson.entities;

public class Personne {

    private Long idPerson;
    private String name;
    private String familyName;
    private String adress;
    private String mail;
    private String phone;
    public Personne() {
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getAdress() {
        return adress;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
